function sayHello() {
  console.log("Chào bạn");
  console.log("Have a good day");
}
sayHello();
sayHello();
sayHello();

// hàm có tham số

function sayHelloByName(user) {
  console.log("Chào " + user);
}

sayHelloByName("Alice");
sayHelloByName("Bob");

var diemToan = 2;
var diemVan = 2.5;
// không có giá trị return
// function tinhDiemTB(diemToan, diemVan) {
//   var dtb = (diemToan + diemVan) / 2;
//   console.log("dtb: ", dtb);

// }
// function tinhDiemTB(diemToan, diemVan) {
//   var dtb = (diemToan + diemVan) / 2;
//   console.log("dtb: ", dtb);
//   return dtb;
//   console.log("yes");
// }

var tinhDiemTB = function () {
  var dtb = (diemToan + diemVan) / 2;
  console.log("dtb: ", dtb);
  return dtb;
  console.log("yes");
};

tinhDiemTB(diemToan, diemVan);

var diemTB = tinhDiemTB(5);
console.log("diemTB: ", diemTB);
