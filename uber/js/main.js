const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";
const UBER_CAR = "uberCar";

function tinhGiaTienKmDauTien(car) {
  //   console.log("car", car);
  var giaTien = 0;
  if (car == UBER_CAR) {
    giaTien = 8000;
  }
  if (car == UBER_SUV) {
    giaTien = 9000;
  }
  if (car == UBER_BLACK) {
    giaTien = 10000;
  }
  return giaTien;
}

function tinhGiaTiemKm1_19(car) {
  if (car == UBER_CAR) {
    return 7500;
  }
  if (car == UBER_SUV) {
    return 8500;
  }
  if (car == UBER_BLACK) {
    return 9500;
  }
}
function tinhGiaTienKm19TroDi(car) {
  switch (car) {
    case UBER_CAR: {
      return 7000;
    }
    case UBER_SUV: {
      return 8000;
    }
    case UBER_BLACK: {
      return 9000;
    }
  }
}

function tinhTienUber() {
  //   console.log("yes");
  //    lấy loại xe user chọn
  var carValue = document.querySelector('input[name="selector"]:checked').value;
  //   console.log("carValue: ", carValue);
  //  lấy giá tiền theo từng mốc km của từng loại xe
  var giaTienKmDauTien = tinhGiaTienKmDauTien(carValue);
  var giaTienKm1_19 = tinhGiaTiemKm1_19(carValue);
  var giaTienKm19TroDi = tinhGiaTienKm19TroDi(carValue);

  var kmValue = document.getElementById("txt-km").value * 1;
  console.log("kmValue: ", kmValue);

  var result = 0;

  if (kmValue <= 1) {
    result = kmValue * giaTienKmDauTien;
  } else if (kmValue <= 19) {
    result = giaTienKmDauTien * 1 + (kmValue - 1) * giaTienKm1_19;
  } else {
    result =
      giaTienKmDauTien * 1 +
      18 * giaTienKm1_19 +
      (kmValue - 19) * giaTienKm19TroDi;
  }
  console.log("result: ", result.toLocaleString());
  //   console.log("giaTienKm19TroDi: ", giaTienKm19TroDi);
  //   console.log("giaTienKm1_19: ", giaTienKm1_19);
  //   console.log("giaTienKmDauTien: ", giaTienKmDauTien);
}

// 17km SUV
